<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザー情報編集</title>
</head>
<body>
	<p>ユーザー情報編集</p>
	<div class="main-contents">
		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="message">
						<li><c:out value="${message}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>
		<form action="SettingServlet" method="post">
			<br /> <input name="id" value="${settingUser.id}" id="id"
				type="hidden" />
			<ul>
				<li><label for="login_id">LOGIN ID</label> <input
					name="login_id" value="${settingUser.login_id}" /></li>
				<li>パスワード<input name="password" type="password" id="password" /></li>
				<li>確認用パスワード <input name="password2" type="password"id="password2" /></li>
				<li><label for="name">名前</label> <input name="name"value="${settingUser.name}" id="name" /></li>
				<li>支店<select name="branch" size="1" id="branches">
						<c:forEach items="${branches}" var="br">
							<c:if test="${settingUser.branch == br.id}">
								<option value="${br.id}" selected>${br.branch}</option>
							</c:if>
							<c:if test="${settingUser.branch != br.id}">
								<option value="${br.id}">${br.branch}</option>
							</c:if>
						</c:forEach>
				</select></li>
				<!-- itemsはListの変数名に合わせる -->
				<li>部署<select name="department" size="1" id="departments">
						<c:forEach items="${departments}" var="depa">
							<c:if test="${settingUser.department == depa.id}">
								<option value="${depa.id}" selected>${depa.department}</option>
							</c:if>
							<c:if test="${settingUser.department != depa.id}">
								<option value="${depa.id}">${depa.department}</option>
							</c:if>
						</c:forEach>
				</select></li>
			</ul>
			<input type="submit" value="登録" /> <br /> <a href="./">戻る</a>
		</form>
	</div>
</body>
</html>