<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザー登録</title>
</head>
<body>
	<h1>ユーザー登録</h1>
	<div class="main-contents">
		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="message">
						<li><c:out value="${message}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>

		<form action="RegisterServlet" method="post">
			<ul>
				<li>ログインID:<input type="text" name="login_id" value="${login_id}"></li>
				<li>パスワード:<input type="password" name="password" value="${password}"></li>
				<li>確認用パスワード <input name="password2" type="password" id="password2" /></li>
				<li>名称:<input type="text" name="name" value="${name}"></li>
				<li>支店<select name="branch" size="1" id="branches">
						<c:forEach items="${branches}" var="br">
							<option value="${br.id}">${br.branch}</option>
						</c:forEach>
				</select></li>
				<!-- itemsはListの変数名に合わせる -->
				<li>部署<select name="department" size="1" id="departments">
						<c:forEach items="${departments}" var="depa">
							<option value="${depa.id}">${depa.department}</option>
						</c:forEach>
				</select></li>
			</ul>
			<!-- 登録 -->
			<input type="submit" value="登録" />
			<a href="./main.jsp">戻る</a>
		</form>
	</div>
</body>
</html>