<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ホーム画面</title>
</head>
<body>
	<h1>ホーム画面</h1>
	<p>ユーザー情報</p>
	<table border="1">
		<tr>
			<th>ログインID</th>
			<th>名前</th>
			<th>部署</th>
			<th>役職</th>
		</tr>
		<c:forEach items="${userList}" var="users">
			<tr>
				<th>${users.login_id }</th>
				<th>${users.name }</th>
				<th>${users.branchName }</th>
				<th>${users.departmentName  }</th>
				<th><a href="SettingServlet?id=${users.id}">編集</a></th>
			</tr>
		</c:forEach>
	</table>
	<p>
		<a href="RegisterServlet">新規ユーザー登録</a>
	</p>
</body>
</html>