package beans;

public class Branches {

	private int id;
	private String branch;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getBranch() {
		return branch;
	}

	public void setBranchName(String branch) {
		this.branch = branch;
	}

}
