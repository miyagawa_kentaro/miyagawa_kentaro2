package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.Branches;
import exception.SQLRuntimeException;

public class BranchDao {

	public List<Branches> select(Connection connection) {

        PreparedStatement pst = null;

	        try {
	            StringBuilder sql = new StringBuilder();
	            sql.append("SELECT ");
	            sql.append("branches.id as id, ");
	            sql.append("branches.branch_name as branch_name ");
	            sql.append("FROM branches ;");

	            pst = connection.prepareStatement(sql.toString());

	            ResultSet rs = pst.executeQuery();
	            List<Branches> ret = toBranchList(rs);
	            return ret;

	        } catch (SQLException e) {
	            throw new SQLRuntimeException(e);
	        } finally {
	            close(pst);
	        }
    }

	private List<Branches> toBranchList(ResultSet rs)
            throws SQLException {

        List<Branches> ret = new ArrayList<Branches>();
	        try {
	            while (rs.next()) {
	                String branch_name = rs.getString("branch_name");
	               int id = rs.getInt("id");

	                Branches branch = new Branches();
	                branch.setBranchName(branch_name);
	                branch.setId(id);

	                ret.add(branch);
	            }
	            return ret;
	        } finally {
	            close(rs);
	        }
    }
}
