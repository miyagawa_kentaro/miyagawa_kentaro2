package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.Departments;
import exception.SQLRuntimeException;

public class DepartmentDao {

	public List<Departments> select(Connection connection) {

		 PreparedStatement pst = null;
	        try {
	            StringBuilder sql = new StringBuilder();
	            sql.append("SELECT ");
	            sql.append("departments.id as id, ");
	            sql.append("departments.department_name as department_name ");
	            sql.append("FROM departments; ");

	            pst = connection.prepareStatement(sql.toString());

	            ResultSet rs = pst.executeQuery();
	            List<Departments> ret = toDepartmentList(rs);
	            return ret;
	        } catch (SQLException e) {
	            throw new SQLRuntimeException(e);
	        } finally {
	            close(pst);
	        }
	}

	 private List<Departments> toDepartmentList(ResultSet rs)
	            throws SQLException {

	        List<Departments> ret = new ArrayList<Departments>();
	        try {
	            while (rs.next()) {
	                String department_name = rs.getString("department_name");
	                int id = rs.getInt("id");

	                Departments department = new Departments();
	                department.setDepartment(department_name);
	                department.setId(id);

	                ret.add(department);
	            }
	            return ret;
	        } finally {
	            close(rs);
	        }
	 }

}
