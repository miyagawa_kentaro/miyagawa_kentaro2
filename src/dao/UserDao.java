package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServlet;

import org.apache.commons.lang.StringUtils;

import beans.User;
import exception.NoRowsUpdatedRuntimeException;
import exception.SQLRuntimeException;

public class UserDao extends HttpServlet{

	public void insert(Connection connection, User user) {

		PreparedStatement pst = null;
		//データベースの接続
			try {
				//SQL文を作成（insert）
				String sql = "INSERT INTO users (login_id,password,name,branch_id,department_id) VALUES(?,?,?,?,?)";
				//sqlを送信する
				pst = connection.prepareStatement(sql);

				pst.setString(1, user.getLogin_id());
				pst.setString(2, user.getPassword());
				pst.setString(3, user.getName());
				pst.setInt(4, user.getBranch());
				pst.setInt(5, user.getDepartment());

				//sqlの実行結果を取得
				pst.executeUpdate();

			} catch (SQLException e) {
				// 接続エラー時はコンソールに出力
				throw new SQLRuntimeException(e);

			}finally {
			close(pst);
			}
	}//insert()

	public User settingUser(Connection connection,int id) {

		  PreparedStatement pst = null;
		    try {
		        String sql = "select * from users inner join branches on users.branch_id = branches.id "
		        		+ " inner join departments on users.department_id = departments.id WHERE users.id = ?";

		        pst = connection.prepareStatement(sql);
		        pst.setInt(1,id);

		        ResultSet rs = pst.executeQuery();
		        List<User> userList = toUserList(rs);
		        if (userList.isEmpty() == true) {
		            return null;
		        } else if (2 <= userList.size()) {
		            throw new IllegalStateException("2 <= userList.size()");
		        } else {
		            return userList.get(0);
		        }
		    } catch (SQLException e) {
		        throw new SQLRuntimeException(e);
		    } finally {
		        close(pst);
		    }
	}


	private List<User> toUserList(ResultSet rs) throws SQLException {

		List<User> ret = new ArrayList<User>();
			try {
				while (rs.next()) {
					int id = rs.getInt("id");
					String loginId = rs.getString("login_id");
		            String password = rs.getString("password");
		            String name = rs.getString("name");
		            int branchID = rs.getInt("branch_id");
		            int departmentID = rs.getInt("department_id");
		            Timestamp createdDate = rs.getTimestamp("created_date");
		            Timestamp updatedDate = rs.getTimestamp("updated_date");
		            String branchName = rs.getString("branch_name");
		            String departmentName = rs.getString("department_name");

		            User user = new User();
		            user.setId(id);
		            user.setLogin_id(loginId);
		            user.setPassword(password);
		            user.setName(name);
		            user.setBranch(branchID);
		            user.setDepartment(departmentID);
		            user.setCreatedDate(createdDate);
		            user.setUpdatedDate(updatedDate);
		            user.setBranchName(branchName);
		            user.setDepartmentName(departmentName);

		            ret.add(user);
		        }
		        return ret;
		    } finally {
		        close(rs);
		    }
	}


	public void update(Connection connection, User user) {

	    PreparedStatement pst = null;
		    try {
		        StringBuilder sql = new StringBuilder();

		        sql.append("UPDATE users SET");
		        sql.append("  login_id = ?");
		        sql.append(", name = ?");
		        sql.append(", branch_id = ?");
		        sql.append(", department_id = ?");
		        if(StringUtils.isEmpty(user.getPassword())==false) {
		            sql.append(", password = ?");
		            }
		        sql.append(" WHERE");
		        sql.append(" id = ?");

		        pst = connection.prepareStatement(sql.toString());

		        pst.setString(1, user.getLogin_id());
		        pst.setString(2, user.getName());
		        pst.setInt(3, user.getBranch());
		        pst.setInt(4, user.getDepartment());
		        if(StringUtils.isEmpty(user.getPassword())==false) {
		            pst.setString(5, user.getPassword());
		            pst.setInt(6, user.getId());
		        }else {
		            pst.setInt(5, user.getId());
		        }
		        int count = pst.executeUpdate();
		        if (count == 0) {
		            throw new NoRowsUpdatedRuntimeException();
		        }
		    } catch (SQLException e) {
		        throw new SQLRuntimeException(e);
		    } finally {
		        close(pst);
		    }
	}
	public List<User> select(Connection connection) {

	    PreparedStatement pst = null;

		    try {
		        StringBuilder sql = new StringBuilder();
		        sql.append("SELECT * FROM users ");
		        sql.append("INNER JOIN ");
		        sql.append("branches ");
		        sql.append("ON ");
		        sql.append("users.branch_id = branches.id ");
		        sql.append("INNER JOIN ");
		        sql.append("departments ");
		        sql.append("ON ");
		        sql.append("users.department_id = departments.id;");

		        pst = connection.prepareStatement(sql.toString());

		        ResultSet rs = pst.executeQuery();
		        List<User> ret = toUserList(rs);
		        return ret;

		    } catch (SQLException e) {
		        throw new SQLRuntimeException(e);
		    } finally {
		        close(pst);
		    }
	}



}//class
