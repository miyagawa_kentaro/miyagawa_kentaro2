package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import beans.Branches;
import dao.BranchDao;

public class BranchService {


	public List<Branches> getBranch() {

		Connection connection = null;
			try {
				connection = getConnection();

				BranchDao branchDao = new BranchDao();
				List<Branches> ret = branchDao.select(connection);

				commit(connection);

				return ret;
			}catch (RuntimeException e) {
				rollback(connection);
				e.printStackTrace();
				throw e;
			} catch (Error e) {
				rollback(connection);
				e.printStackTrace();
				throw e;
			} finally {
				close(connection);
			}
	}
}
