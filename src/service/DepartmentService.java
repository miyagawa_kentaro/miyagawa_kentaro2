package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import beans.Departments;
import dao.DepartmentDao;

public class DepartmentService {


	public List<Departments> getDepartment() {

		Connection connection = null;
			try {
				connection = getConnection();

				DepartmentDao departmentDao = new DepartmentDao();
				List<Departments> ret = departmentDao.select(connection);

				commit(connection);

				return ret;

			}catch (RuntimeException e) {
				rollback(connection);
				throw e;
			} catch (Error e) {
				rollback(connection);
				throw e;
			} finally {
				close(connection);
			}
	}

}
