package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Branches;
import beans.Departments;
import beans.User;
import service.BranchService;
import service.DepartmentService;
import service.UserService;



@WebServlet("/RegisterServlet")
public class RegisterServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		List<Branches> branch = new BranchService().getBranch();
    	request.setAttribute("branches", branch);
    	//"branches"はbranchに入っている情報をURLにしたもの
    	List<Departments> department = new DepartmentService().getDepartment();
    	request.setAttribute("departments", department);
    	//setAttributeはカプセル化してbeansに一時的に収納する機能
		request.getRequestDispatcher("userRegister.jsp").forward(request, response);
	 }

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		 List<String> messages = new ArrayList<String>();

		 HttpSession session = request.getSession();

		 String loginId = request.getParameter("login_id");
	     String password = request.getParameter("password");
	     String name = request.getParameter("name");
	     List<Branches> branch = new BranchService().getBranch();
	     List<Departments> department = new DepartmentService().getDepartment();

	        if (isValid(request, messages) == true) {

	            User user = new User();
	            user.setLogin_id(request.getParameter("login_id"));
	            user.setPassword(request.getParameter("password"));
	            user.setPassword2(request.getParameter("password2"));
	            user.setName(request.getParameter("name"));
	            user.setBranch(Integer.parseInt(request.getParameter("branch")));
	            user.setDepartment(Integer.parseInt(request.getParameter("department")));

	            new UserService().register(user);

	            response.sendRedirect("./");
	        } else {
	        	 session.setAttribute("errorMessages", messages);
	        	 session.setAttribute("login_id", loginId);
	        	 session.setAttribute("password", password);
	        	 session.setAttribute("name", name);
	        	 request.setAttribute("branches", branch);
	        	 request.setAttribute("departments", department);
	        	 request.getRequestDispatcher("/userRegister.jsp").forward(request, response);
	        }
	}

	    private boolean isValid(HttpServletRequest request, List<String> messages) {

	    	String loginId = request.getParameter("login_id");
	        String password = request.getParameter("password");
	        String password2 = request.getParameter("password2");
	        String name = request.getParameter("name");

	        if (StringUtils.isEmpty(loginId) == true) {
	            messages.add("ログインIDを入力してください");
	        }
	        if (!loginId.matches("^[a-zA-Z0-9]{6,20}$")) {
	        	messages.add("ログインIDは半角英数字6文字以上20文字以下です");
	        }
	        if (!password.matches("^[a-zA-Z0-9 -~]{6,20}$")) {
		        messages.add("パスワードは記号を含む全ての半角文字で6文字以上20文字以下です");

		    }else if (!password.equals(password2)) {
	        	messages.add("パスワードが一致しません");
	        }
	        if (StringUtils.isEmpty(name) == true) {
	        	messages.add("名前を入力してください");
	        }
	        if (name.length()>10) {
	        	messages.add("名前は10文字以内です");
	        }
	        if (messages.size() == 0) {
	            return true;
	        } else {
	            return false;
	        }
	    }
}
